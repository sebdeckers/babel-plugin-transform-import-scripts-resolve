import test from 'ava'
import {transform} from 'babel-core'
import transformImportScriptsResolve from '..'

const fixtures = [
  {
    label: 'Single import argument',
    source: `importScripts('./foo.js');`,
    outputDirectory: `${process.cwd()}`,
    sourceToDestination: new Map([
      [`${process.cwd()}/foo.js`, `${process.cwd()}/bar.js`]
    ]),
    expected: `importScripts('./bar.js');`
  },
  {
    label: 'Multiple import arguments',
    source: `importScripts('./foo.js', './bar.js');`,
    outputDirectory: `${process.cwd()}`,
    sourceToDestination: new Map([
      [`${process.cwd()}/foo.js`, `${process.cwd()}/abc.js`],
      [`${process.cwd()}/bar.js`, `${process.cwd()}/def.js`]
    ]),
    expected: `importScripts('./abc.js', './def.js');`
  },
  {
    label: 'Plain object as map',
    source: `importScripts('./foo.js');`,
    outputDirectory: `${process.cwd()}`,
    sourceToDestination: {
      [`${process.cwd()}/foo.js`]: `${process.cwd()}/bar.js`
    },
    expected: `importScripts('./bar.js');`
  },
  {
    label: 'Function as map',
    source: `importScripts('./foo.js');`,
    outputDirectory: `${process.cwd()}`,
    sourceToDestination: function () {
      return `${process.cwd()}/bar.js`
    },
    expected: `importScripts('./bar.js');`
  },
  {
    label: 'Re-mapped output directory',
    source: `importScripts('./foo.js');`,
    outputDirectory: `${process.cwd()}/public`,
    sourceToDestination: new Map([
      [`${process.cwd()}/foo.js`, `${process.cwd()}/public/bar.js`]
    ]),
    expected: `importScripts('./bar.js');`
  }
]

for (const {label, source, sourceToDestination, outputDirectory, expected} of fixtures) {
  test(label, (t) => {
    const options = {
      plugins: [
        [transformImportScriptsResolve, {sourceToDestination, outputDirectory}]
      ]
    }
    const actual = transform(source, options)
    t.is(actual.code, expected)
  })
}
