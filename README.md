# babel-plugin-transform-import-scripts-resolve

Map named dependencies to relative paths for `importScripts` calls in Service Worker scripts.

## Usage

### `.babelrc`

```json
{
  "plugins": [
    ["transform-import-resolve", {
      "sourceToDestination": {
        "/some/absolute/foo.js": "/some/absolute/bar.js"
      },
      "outputDirectory": "/public"
    }]
  ]
}
```

### In

```js
importScripts('./foo.js')
```

### Out

```js
importScripts('./bar.js')
```

## Options

### `sourceToDestination`

Type: `Function` or `Object` or `Map`

Maps the absolute path of a dependency to another path.

### `outputDirectory`

Output directory to which the mapped path will be made relative.
