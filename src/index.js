import {dirname, resolve, relative} from 'path'
import relativePathToRelativeUrl from 'relative-path-to-relative-url'

function transformImportScriptsResolve ({types: t}) {
  return {
    visitor: {
      CallExpression (
        path,
        {
          opts: {sourceToDestination, outputDirectory},
          file: {opts: {filename}}
        }
      ) {
        if (t.isIdentifier(path.node.callee, {name: 'importScripts'})) {
          const basedir = dirname(filename)
          for (const argument of path.get('arguments')) {
            if (t.isStringLiteral(argument)) {
              const dependency = resolve(basedir, argument.node.value)
              const mapped = typeof sourceToDestination.get === 'function' ? sourceToDestination.get(dependency)
                : typeof sourceToDestination === 'function' ? sourceToDestination(dependency)
                : sourceToDestination[dependency]
              const revved = relative(outputDirectory, mapped)
              argument.replaceWith(
                t.stringLiteral(relativePathToRelativeUrl(revved))
              )
            }
          }
        }
      }
    }
  }
}

module.exports = transformImportScriptsResolve
